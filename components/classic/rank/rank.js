// componted/rank/rank.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    index:{
      type:Number,
      observer(newvalue){
        let value=newvalue<10 ? "0"+newvalue :newvalue;
        this.setData({
          _index:value
        })
      }
    }
  },
  lifetimes: {
    attached: function() {
     let date=new Date();
     let m=date.getMonth();
     let y=date.getFullYear();
     this.setData({
       month:m,
       year:y
     })
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  /**
   * 组件的初始数据
   */
  data: {
    _index:"",
    month:0,
    year:0,
    months:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","是一月","十二月"]
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
