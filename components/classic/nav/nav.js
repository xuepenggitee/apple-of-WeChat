// components/nav/nav.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title:{
      type:String
    },
    first:{
      type:Boolean
    },
    last:{
      type:Boolean
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    leftImg:"./images/triangle@left.png",
    disleftImg:"./images/triangle.dis@left.png",
    rightImg:"./images/triangle@right.png",
    disrightImg:"./images/triangle.dis@right.png"
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onleft(){
      if(!this.properties.first){
        this.triggerEvent("onleft")
      }
    },
    onright(){
      if(!this.properties.last){
        this.triggerEvent("onright")
      }
    }
  }
})
