// components/classic/music/music.js
import {bh} from "../classic_bh"
const music=wx.getBackgroundAudioManager();
Component({
  /**
   * 组件的属性列表
   */
  behaviors: [bh],
  properties: {
    url:{
      type:String
    },
    constent:{
      type:String
    }
  },
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      this.music_status()
      this.music_switch()
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  /**
   * 组件的初始数据
   */
  data: {
    yesImg:"/images/music/播放中.png",
    noImg:"/images/music/暂停中.png",
    playing:false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    play(){
     let play=!this.data.playing;
     console.log(play)
     this.setData({
       playing:play
     })
     if(play){
       music.title=this.properties.content
       music.src=this.properties.url
     }else{
       music.pause()
     }
    },
    music_status(){
      music.onPlay(()=>{
        this.setData({
          playing:true
        })
      })
      music.onPause(()=>{
        this.setData({
          playing:false
        })
      })
      music.onEnded(()=>{
        this.setData({
          playing:false
        })
      })
      music.onStop(()=>{
        this.setData({
          playing:false
        })
      })
    },
    music_switch(){
      if(music.paused) return
      if(this.properties.url==music.src){
        this.setData({
          playing:true
        })
      }
    }
  }
})
