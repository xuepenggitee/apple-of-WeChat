// components/like/like.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    like_status:{
      type:Number
    },
    fav_nums:{
      type:Number
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    yesImg:"/images/like/喜欢-拷贝@2x的副本.png",
    noImg:"/images/like/喜欢@2x.png"
  },
 
  /**
   * 组件的方法列表
   */
  methods: {
    onLike(){
      let like_status=this.properties.like_status;
      let fav_nums=like_status ? this.properties.fav_nums-1: this.properties.fav_nums+1
      this.setData({
        like_status:!like_status,
        fav_nums:fav_nums
      }),
      this.triggerEvent("onLike",{like_status:!like_status})
    }
  }
})
