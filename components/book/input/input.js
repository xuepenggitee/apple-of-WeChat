// components/book/search/search.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    show:true,
    value:""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    search(){
     this.setData({
       show:false
     })
    },
    tonav(){
      wx.navigateTo({
        url:"/pages/book-search/book-search",
      })
    }
  }
})
