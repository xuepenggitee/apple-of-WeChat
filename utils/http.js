import {config} from '../config'

const errorTigs={
    "1000":	"输入参数错误",
    "1001":	"输入的json格式不正确",
    "1002":	"找不到资源",
    "1003":	"未知错误",
    "1004":	"禁止访问",
    "1005":	"不正确的开发者key",
    "1006":	"服务器内部错误",
    "301":	"永久重定向",
    "400":	"请求包含不支持的参数",
    "401":	"未授权",
    "403":	"被禁止访问",
    "404":	"请求的资源不存在",
    "413":	"上传的File体积太大",
    "500":	"内部错误",
  }

export class Http {
  showError(code){
    wx.showToast({
      title:errorTigs[code],
      icon:'error',
      duration:2000
    })
  }
  request(url,method="GET",data={}){
    console.log("test")
    let that=this;
    let promise=new Promise((resolve)=>{
      wx.request({
        url:"http://bl.talelin.com/v1"+url,
        method:method,
        data:data,
        header:{
          appkey:config.appkey,
          "Content-Type":"application/json"  
        },
        success(res){
         let statusCode=res.statusCode.toString();
         console.log(res)
         if(statusCode.startsWith("2")){
           resolve(res)
         }else{
           that.showError(statusCode)
         }
        },
        fail(res){
          console.log(res)
        }
        })

    })
    return promise
  }
}