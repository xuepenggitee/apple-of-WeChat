import {Http} from '../utils/http'
export class Like extends Http{
  getLikeStatus(type,id){
     return this.request(`/classic/${type}/${id}/favor`)
  }
  getLike(id,type,like){
    let url=like ?  "/like" : "/like/cancel"
    this.request(
     url,
     "POST",
     {
       art_id:id,
       type:type
     }
    )
  }
}