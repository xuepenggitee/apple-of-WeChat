import {Http} from "../utils/http"
export class Classic extends Http{
  getlatest(){
    let that=this;
   return this.request("/classic/latest")
  }
  getPrevOrNext(index,prevOrNext){
    let that=this
    let key=prevOrNext=="next"?index+1:index-1;
    let StorageClassic=this.getStorage(key)
    if(StorageClassic){
      return new Promise((resolve)=>{
        resolve(StorageClassic)
      })
    }else{
      let p=this.request(`/classic/${index}/${prevOrNext}`)
      p.then(res=>{
        that.setStorage(res)
      })
      return p
  }}
  judFirst(index){
    let first=wx.getStorageSync('first')
    return index==first
  }
  judLast(index){
    return index==1
  }
  setStorage(res,item="classic-"){
    wx.setStorageSync(item+res.data.index,res)
  }
  getStorage(key,item="classic-"){
    return wx.getStorageSync(item+key)
  }
}