import {Http} from "../utils/http"
export class Book extends Http{
  getHotList(){
  return  this.request("/book/hot_list")
  }
  getdetail(uid){
   return this.request(`/book/${uid}/detail`)
  }
  getComments(uid) {
   return this.request(`/book/${uid}/short_comment`)
  }
  getLike(uid){
   return this.request(`/book/${uid}/favor`)
  }
  addComment(id,content){
    this.request(
      "/book/add/short_comment",
      "POST",
      {
        book_id: id,
        content:content
      },
    )
  }
  valueNull(){
    wx.showToast({
      title: '输入不能为空',
      icon:"error"
    })
  }
  getHotKeyword(){
    return this.request("/book/hot_keyword")
   }
   search(data){
     return this.request(
       `/book/search`,"GET",data,
     )
   }
   loading(){
    wx.showLoading({
      title: '加载中',
    })
  }
  loadOver(){
    wx.hideLoading()
  }
}