
export class Keyword{
    key="keyword"
    getHistory(){
      let words = wx.getStorageSync(this.key)
      if(!words){
        return []
      }
      return words
    }
    setHistory(value){
      let words=this.getHistory()
      let length=words.length
      let has=words.includes(value)
      if(!has){
        if (length >=8) {
           words.pop()
        }
        words.unshift(value)
      }    
      wx.setStorageSync(this.key, words)
    }
}