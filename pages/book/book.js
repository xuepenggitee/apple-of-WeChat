// pages/book/book.js
import {Book} from "../../models/book"
let book=new Book();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    data:[],
    total:0,
    more:0,
    show:true,
    content:"",
  },
  search(content){
    book.loading()
    let that=this
    let length=this.data.data.length;
    let data={
      start:length,
      count:20,
      summary:0,
      q: content
    }
    let searchResult=this.data.data;
    book.search(data).then(res=>{
      console.log(res)
      that.setData({
        data:searchResult.concat(res.data.books),
        total:res.data.total,
        show:false,
        end:false,
        more:searchResult.concat(res.data.books).length
      })
      book.loadOver()
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
 
  onLoad(options) {
    book.loading()
    let content=wx.getStorageSync('content')
    if(!content){
      let that=this
      book.getHotList().then(res=>{
        that.setData({
          data:res.data,
          show:true
        })
        book.loadOver()
      })
    }else{
      this.search(content)
      this.setData({
        content:content
      })
      wx.removeStorageSync('content')
      book.loadOver()
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
   
  let more=this.data.data.length;
  let total=this.data.total
  let content=this.data.content
  if(total>more){
   this.search(content)
  }
},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})