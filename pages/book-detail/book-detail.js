// pages/book-detail/book-detail.js
import {Book} from "../../models/book"
import {Like} from "../../models/like"
let book=new Book()
let like=new Like()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail:{},
    comments:[],
    fav_nums:0,
    like_status:1,
    posting:false,
    value:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLike(e){
    let like_status = e.detail.like_status
    let id = this.data.detail.id
    let type = 400
    like.getLike(id,type,like_status)
  },
  onLoad(options) {
    book.loading()
    let that=this
    let uid=options.uid;
    let detail=book.getdetail(uid)
    let comments=book.getComments(uid)
    let like_status=book.getLike(uid)
    Promise.all([detail,comments,like_status]).then(res=>{
      that.setData({
          detail:res[0].data,
          comments:res[1].data.comments,
          fav_nums:res[2].data.fav_nums,
          like_status:res[2].data.like_status
        })
        book.loadOver()
    })


    // book.getdetail(uid).then(res=>{
    //   console.log("data",res.data)
    //   that.setData({
    //    detail:res.data
    //   })
    // })
     
    //   book.getComments(uid,res=>{
    //     wx.hideLoading()
    //      that.setData({
    //       comments:res.data.comments
    //      })
    //    })

   
    // book.getLike(uid,res=>{
    //   console.log("like",res.data)
    //   that.setData({
    //     fav_nums:res.data.fav_nums,
    //     like_status:res.data.like_status
    //   })
    // })
  },
 showposting(){
   let posting=this.data.posting
   this.setData({
     posting:!posting,
     value:""
   })
 },
 setValue(e){
let value=this.data.value
let html=e.currentTarget.dataset.html
this.setData({
   value:value+html
 })
 },
 addCommnet(){
 book.loading()
  let that=this;
  let id = this.data.detail.id;
  let value=this.data.value.slice(0,12);
  
  if(value){
    this.data.comments.unshift({
      content: value,
      nums:1
    })
    book.addComment(id,value)
    book.loadOver()
    that.showposting(),
    that.setData({
      comments:this.data.comments,
    })
  }else{
    book.valueNull()
  }
 
 },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})