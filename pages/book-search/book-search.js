// pages/book-search/book-search.js
import {Keyword} from "../../models/keyword"
import {Book} from "../../models/book"
let keyword=new Keyword()
let book=new Book()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searching:true,
    hot:[],
    history:[],
    value:"",
    deleteshow:false
  },
  deleteValue(){
    this.setData({
      value:""
    })
  },
  deleteshow(){
    this.setData({
      deleteshow:true
    })
  },
  getValue(e){
this.setData({
  value:e.detail.value
})
  },
  /**
   * 生命周期函数--监听页面加载
   */
  ontap(e){
    let content=e.detail.content||this.data.value;
    if(content){
      wx.setStorageSync("content",content)
      keyword.setHistory(content)
      wx.redirectTo({
        url: `/pages/book/book`
      })
    }else{
      book.valueNull()
    }
  },
  toback(){
    wx.redirectTo({
      url: "/pages/book/book"
    })
  },
  onLoad(options) {
    let history=keyword.getHistory();
    let that=this
    book.getHotKeyword().then(res=>{
      that.setData({
        hot:res.data.hot,
        history:history
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
 
  },
  
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
   
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})