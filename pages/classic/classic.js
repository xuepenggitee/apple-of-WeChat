// pages/classic/classic.js
import {Classic} from "../../models/classic";
import {Like} from "../../models/like"
const like=new Like()
const classic=new Classic()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    data:{},
    fav_num:0,
    like_status:1,
    first:true,
    last:false,
  },
  getLikeStatus(res){
    let that=this
    like.getLikeStatus(res.type,res.id).then(res=>{
      that.setData({
        fav_nums:res.data.fav_nums,
        like_status:res.data.like_status
      })
    })
  },
  onLike(e){
    let id=this.data.data.id;
    let type=this.data.data.type;
    let like_status=e.detail.like_status;
   like.getLike(id,type,like_status)
  },
  onright(){
    this.getPrevOrNext("previous")
  },
  onleft(){
this.getPrevOrNext("next")
  },
  getPrevOrNext(prevOrNext){
    let that=this;
    let index=this.data.data.index;
    classic.getPrevOrNext(index,prevOrNext).then(res=>{
      console.log(res)
      that.getLikeStatus(res.data)
      that.setData({
        data:res.data,
        first:classic.judFirst(res.data.index),
        last:classic.judLast(res.data.index)
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let that=this
    classic.getlatest().then(res=>{
         wx.setStorageSync('first', res.data.index)
      that.getLikeStatus(res.data)
     that.setData({
       data:res.data
     })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})